﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThermalConductivity.Enums;

namespace ThermalConductivity.ViewModels
{
    public class DropControlViewModel : BaseViewModel
    {
        #region [Fields]

        DropSelectedSide selectedSide;

      

        #endregion // [Fields]

        #region [Constructors]

        #endregion //[Constructors]

        #region [Properties]

        public DropSelectedSide SelectedSide
        {
            get { return selectedSide; }
            set 
            {
                selectedSide = value;
                OnPropertyChanged("SelectedSide");
            }
        }

        #endregion // [Properties]
    }
}
