﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ThermalConductivity.ViewModels
{
    class ColorScaleViewModel : ControlViewModel
    {

        private const string TITLE = "Color Scale";

        private static readonly ColorScaleViewModel instance = new ColorScaleViewModel();

        public static ColorScaleViewModel Instance
        {
            get { return instance; }
        }

        public ColorScaleViewModel()
        {
            Title = TITLE;
            IsHiden = false;
            ColorDictionary = new ObservableCollection<KeyValuePair<int, System.Drawing.Color>>();
        }
        ObservableCollection<KeyValuePair<int, System.Drawing.Color>> colorDictionary;

        public ObservableCollection<KeyValuePair<int, System.Drawing.Color>> ColorDictionary
        {
            get { return colorDictionary; }
            set
            {
                colorDictionary = value;
                OnPropertyChanged("ColorDictionary");
            }
        }
    }
}
