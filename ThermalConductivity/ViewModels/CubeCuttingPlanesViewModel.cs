﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using ThermalConductivity.Models;

namespace ThermalConductivity.ViewModels
{
    public class CubeCuttingPlanesViewModel : ControlViewModel
    {
        private const string TITLE = "Cube Cutting Planes";

        private static CubeCuttingPlanesViewModel instance = new CubeCuttingPlanesViewModel();

        public static CubeCuttingPlanesViewModel Instance
        {
            get { return instance; }
        }

        private CubeCuttingPlanesViewModel()
        {
            ColorDictionary = new ObservableCollection<KeyValuePair<int, System.Drawing.Color>>();
            SomeMethod();
            Title = TITLE;
            IsHiden = false;
            XCuttingPlanes = new ObservableCollection<Brush>();
            YCuttingPlanes = new ObservableCollection<Brush>();
            ZCuttingPlanes = new ObservableCollection<Brush>();

        }

        private double[, ,] matrix;
        ObservableCollection<Brush> xCuttingPlanes;
        ObservableCollection<Brush> yCuttingPlanes;
        ObservableCollection<Brush> zCuttingPlanes;


        public double[, ,] Matrix
        {
            get { return matrix; }
            set
            {
                matrix = value;
                OnPropertyChanged("Matrix");
            }
        }

        public ObservableCollection<Brush> XCuttingPlanes
        {
            get { return xCuttingPlanes; }
            set
            {
                xCuttingPlanes = value;
                OnPropertyChanged("XCuttingPlanes");
            }
        }

        public ObservableCollection<Brush> YCuttingPlanes
        {
            get { return yCuttingPlanes; }
            set
            {
                yCuttingPlanes = value;
                OnPropertyChanged("YCuttingPlanes");
            }
        }

        public ObservableCollection<Brush> ZCuttingPlanes
        {
            get { return zCuttingPlanes; }
            set
            {
                zCuttingPlanes = value;
                OnPropertyChanged("ZCuttingPlanes");
            }
        }

       public void TemperatureMatrixChanged(object sender, EventArgs e)
        {
            matrix = (sender as CubeModel).TemperatureMatrix;

            XCuttingPlanes.Clear();
            YCuttingPlanes.Clear();
            ZCuttingPlanes.Clear();
            System.Drawing.Bitmap b = new System.Drawing.Bitmap(50, 50);
            for (int z = 1; z < 5; z++)
            {
                for (int i = 0; i < 50; i++)
                    for (int j = 0; j < 50; j++)
                        b.SetPixel(i, j, GetColorByTemperature((int)matrix[z * 10 - 1,i, j]));
                XCuttingPlanes.Add(new ImageBrush(BitmapHelper.LoadBitmap(b)));
            }
            for (int z = 1; z < 5; z++)
            {
                for (int i = 0; i < 50; i++)
                    for (int j = 0; j < 50; j++)
                        b.SetPixel(i, j, GetColorByTemperature((int)matrix[i, z * 10 - 1,j]));
                YCuttingPlanes.Add(new ImageBrush(BitmapHelper.LoadBitmap(b)));
            }
            for (int z = 1; z < 5; z++)
            {
                for (int i = 0; i < 50; i++)
                    for (int j = 0; j < 50; j++)
                        b.SetPixel(i, j, GetColorByTemperature((int)matrix[i, j, z * 10 - 1]));
                ZCuttingPlanes.Add(new ImageBrush(BitmapHelper.LoadBitmap(b)));
            }

        }
        private System.Drawing.Color GetColorByTemperature(int temperature)
        {

            for (int i = 0; i < ColorDictionary.Count; i++)
            {
                if (temperature < 2)
                    return System.Drawing.Color.Blue;
                if (temperature >= 80)
                    return System.Drawing.Color.Red;
                if (temperature >= ColorDictionary[i].Key)
                    return ColorDictionary[i - 1].Value;

            }
            return System.Drawing.Color.Black;
        }

        ObservableCollection<KeyValuePair<int, System.Drawing.Color>> colorDictionary;
        public ObservableCollection<KeyValuePair<int, System.Drawing.Color>> ColorDictionary
        {
            get { return colorDictionary; }
            set
            {
                colorDictionary = value;

            }
        }

        byte r = 250;
        byte g, b;
        bool isLast;

        private void SomeMethod()
        {
            int temperature = 80;

            while (g < 250 && !isLast)
            {
                g += 25;
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                temperature -= 2;

            }
            while (r > 0)
            {
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                r -= 25;
                temperature -= 2;

            }
            while (b < 250)
            {
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                b += 25;
                temperature -= 2;
            }
            while (g > 0)
            {
                isLast = true;
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                g -= 25;
                temperature -= 2;

            }
        }

    }
}
