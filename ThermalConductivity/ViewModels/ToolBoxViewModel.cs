﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts.ModelContracts;

namespace ThermalConductivity.ViewModels
{
    public class ToolBoxViewModel : ControlViewModel
    {
        private const string TITLE = "ToolBox";
        private static ToolBoxViewModel instance = new ToolBoxViewModel();

        private ObservableCollection<IContactBase> items;

        public ObservableCollection<IContactBase> Items
        {
            get { return items; }
            set 
            {
                items = value;
                OnPropertyChanged("Items");
            }
        }

        public static ToolBoxViewModel Instance
        {
            get { return instance; }
        }

        private ToolBoxViewModel()
        {
            Title = TITLE;
            IsHiden = false;
            Items = new ObservableCollection<IContactBase>();

        }


    }
}
