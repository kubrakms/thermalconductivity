﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThermalConductivity.ViewModels
{
    public class PropertyGridViewModel : ControlViewModel
    {
        private const string TITLE = "Property Grid";

        private static PropertyGridViewModel instance = new PropertyGridViewModel();

        public static PropertyGridViewModel Instance
        {
            get { return instance; }
        }

        private PropertyGridViewModel()
        {
            Title = TITLE;
            IsHiden = false;
        }

        private object selectedObject;

        public object SelectedObject
        {
            get { return selectedObject; }
            set 
            {
                selectedObject = value;
                OnPropertyChanged("SelectedObject");
            }
        }

    }
}
