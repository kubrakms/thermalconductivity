﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThermalConductivity.ViewModels
{
   public class BaseViewModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            
            var propertyChanged = PropertyChanged;
            if(propertyChanged!=null)
                propertyChanged(this,new PropertyChangedEventArgs(propertyName));
        }

    }
}
