﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelixToolkit.Wpf;
using ThermalConductivity.Controls;
using ThermalConductivity.Models;
using System.Windows.Media;

namespace ThermalConductivity.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        #region [Fields]

        private ObservableCollection<ControlViewModel> mainWindowControls;

        private ObservableCollection<ControlViewModel> openedWindows;
        private ObservableCollection<ControlViewModel> rightMinimizedWindows;
        private ObservableCollection<ControlViewModel> leftMinimizedWindows;
        private ObservableCollection<ControlViewModel> bottomMinimizedWindows;

        private ControlViewModel selectedItem;

        private DropControlViewModel dropControl;


        private ProjectionViewModel projectionControl;

        #endregion // [Fields]

        #region [Properties]

        public ObservableCollection<ControlViewModel> MainWindowControls
        {
            get { return mainWindowControls; }
            set
            {
                mainWindowControls = value;
                OnPropertyChanged("MainWindowControls");
            }
        }

        public ObservableCollection<ControlViewModel> LeftMinimizedWindows
        {
            get { return leftMinimizedWindows; }
            set
            {
                leftMinimizedWindows = value;
                OnPropertyChanged("LeftMinimizedWindows");
            }
        }

        public ObservableCollection<ControlViewModel> RightMinimizedWindows
        {
            get { return rightMinimizedWindows; }
            set
            {
                rightMinimizedWindows = value;
                OnPropertyChanged("RightMinimizedWindows");
            }
        }

        public ObservableCollection<ControlViewModel> BottomMinimizedWindows
        {
            get { return bottomMinimizedWindows; }
            set
            {
                bottomMinimizedWindows = value;
                OnPropertyChanged("BottomMinimizedWindows");
            }
        }

        public ObservableCollection<ControlViewModel> OpenedWindows
        {
            get { return openedWindows; }
            set
            {
                openedWindows = value;
                OnPropertyChanged("OpenedWindows");
            }
        }

        public ControlViewModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public DropControlViewModel DropControl
        {
            get { return dropControl; }
            set 
            {
                dropControl = value;
                OnPropertyChanged("DropControl");
            }
        }

        public ProjectionViewModel ProjectionControl
        {
            get { return projectionControl; }
            set 
            {
                projectionControl = value;
                OnPropertyChanged("ProjectionControl");
            }
        }


        //private ColorScaleViewModel colorScale;

        //public ColorScaleViewModel ColorScale
        //{
        //    get { return colorScale; }
        //    set 
        //    { 
        //        colorScale = value;
        //        OnPropertyChanged("ColorScale");
        //    }
        //}


        #endregion // [Properties]

        #region [Constructors]

        public MainViewModel()
        {
            MainWindowControls = new ObservableCollection<ControlViewModel>();
            LeftMinimizedWindows = new ObservableCollection<ControlViewModel>();
            RightMinimizedWindows = new ObservableCollection<ControlViewModel>();
            BottomMinimizedWindows = new ObservableCollection<ControlViewModel>();
            OpenedWindows = new ObservableCollection<ControlViewModel>();
          //  ColorScale = new ColorScaleViewModel();
            MainWindowControls.Add(OutputViewModelMain.Instance);
            MainWindowControls.Add(PropertyGridViewModel.Instance);
            MainWindowControls.Add(ToolBoxViewModel.Instance);

            ProjectionControl = new ProjectionViewModel();

            ToolBoxViewModel.Instance.Items.Add(new CubeModel());

            

            ToolBoxViewModel.Instance.Items.Add(new BeCylinderModel());
            ToolBoxViewModel.Instance.Items.Add(new SunLightModel());


            DropControl = new DropControlViewModel();
            DropControl.SelectedSide = Enums.DropSelectedSide.None;

          

           
        }


        #endregion // [Constructors]


    }
}
