﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThermalConductivity.ViewModels
{
    public class OutputViewModelMain : ControlViewModel
    {
        private const string TITLE = "Output";
        private static OutputViewModelMain instance = new OutputViewModelMain();

        public static OutputViewModelMain Instance
        {
            get { return instance; }
        }

        private OutputViewModelMain()
        {
            Title = TITLE;
            IsHiden = false;
        }

        private string message;

        public string Message
        {
            get { return message; }
            set 
            {
                message = value;
                OnPropertyChanged("Message");
            }
        }
    }
}
