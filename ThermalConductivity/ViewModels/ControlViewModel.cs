﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ThermalConductivity.Enums;

namespace ThermalConductivity.ViewModels
{
    public class ControlViewModel : BaseViewModel
    {
        #region [Constants]

        const double MIN_WIDTH = 100;
        const double MIN_HEIGHT = 100;
        const double MAX_WIDTH = 700;
        const double MAX_HEIGHT = 500;

        #endregion // [Constants]

        #region [Fields]

        double left = 100;
        double top = 100;
        double width = 500;
        double height = 300;
        string title;
        bool isHiden = true;
        ControlViewState viewState;

        object content;

        #endregion // [Fields]

        #region [Properties]

        public double Top
        {
            get { return top; }
            set
            {
                top = value;
                OnPropertyChanged("Top");
            }
        }

        public double Left
        {
            get { return left; }
            set
            {
                left = value;
                OnPropertyChanged("Left");
            }
        }

        public double Width
        {
            get { return width; }
            set
            {
                width = value;
                OnPropertyChanged("Width");
            }
        }

        public double Height
        {
            get { return height; }
            set
            {
                height = value;
                OnPropertyChanged("Height");
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged("Title");
            }
        }

        public bool IsHiden
        {
            get { return isHiden; }
            set 
            {
                isHiden = value;
                OnPropertyChanged("IsHiden");
            }
        }

        public ControlViewState ViewState
        {
            get { return viewState; }
            set 
            {
                viewState = value;
                OnPropertyChanged("ViewState");
            }
        }

        public object Content
        {
            get { return content; }
            set
            {
                content = value;
                OnPropertyChanged("Content");
            }
        }

        #endregion // [Properties]

        #region [Methods]
        public void Resize(ResizeOrientation orientation, double horizontalChange, double verticalChange)
        {

            if (!CanResize(Math.Round(width + horizontalChange), Math.Round(height + verticalChange)))
                return;

            switch (orientation)
            {
                case ResizeOrientation.Left:
                    {
                        Left += horizontalChange;
                        Width -= horizontalChange;
                        break;
                    }
                case ResizeOrientation.Right:
                    {
                        Width += horizontalChange;
                        break;
                    }
                case ResizeOrientation.Top:
                    {
                        Top += verticalChange;
                        Height -= verticalChange;
                        break;
                    }
                case ResizeOrientation.Bottom:
                    {
                        Height += verticalChange;
                        break;
                    }
                case ResizeOrientation.RightBottom:
                    {
                        Width += horizontalChange;
                        Height += verticalChange;
                        break;
                    }
            }
        }

        public bool CanResize(double width, double height)
        {
            if (width < MAX_WIDTH &&
                width > MIN_WIDTH &&
                height < MAX_HEIGHT &&
                height > MIN_HEIGHT)
                return true;
            return false;
        }

        #endregion // [Methods]

    }
}
