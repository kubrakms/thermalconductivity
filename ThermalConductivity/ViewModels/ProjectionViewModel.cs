﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Contracts.ModelContracts;
using HelixToolkit.Wpf;

namespace ThermalConductivity.ViewModels
{
    public class ProjectionViewModel : BaseViewModel
    {
        #region [Constructors]

        public ProjectionViewModel()
        {
            Objects3D = new ObservableCollection<Visual3D>();
        }

        #endregion // [Constructors]

        #region [Fields]

        private ObservableCollection<Visual3D> objects3D;
        private IContactBase selectedObject;

        #endregion // [Fields]

        #region [Properties]

        public ObservableCollection<Visual3D> Objects3D
        {
            get { return objects3D; }
            set
            {
                objects3D = value;
                OnPropertyChanged("Objects3D");
            }
        }

        public IContactBase SelectedObject
        {
            get { return selectedObject; }
            set
            {
                selectedObject = value;
                OnPropertyChanged("SelectedObject");
            }
        }

        #endregion // [Properties]

    }
}
