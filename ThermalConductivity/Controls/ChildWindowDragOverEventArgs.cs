﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ThermalConductivity.Controls
{
    public class ChildWindowDragOverEventArgs:EventArgs
    {
        #region [Constructors]

        public ChildWindowDragOverEventArgs(Point position, bool isLeftMouseButtonDown)
        {
            Position = position;
            IsLeftMouseButtonDown = isLeftMouseButtonDown;
        }

        #endregion // [Constructors]

        #region [Properties]

        public bool IsLeftMouseButtonDown { get; set; }
        public Point Position { get; set; }

        #endregion // [Properties]
    }
}
