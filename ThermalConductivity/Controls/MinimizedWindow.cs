﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ThermalConductivity.ViewModels;
using ThermalConductivity.Extension;
using System.Windows.Media;
using ThermalConductivity.Views;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;

namespace ThermalConductivity.Controls
{

    public class DemoElement3D : UIElement3D
    {
        public DemoElement3D()
        {
            var gm = new GeometryModel3D();
            var mb = new MeshBuilder();
            mb.AddSphere(new Point3D(0, 0, 0), 2, 100, 50);
            gm.Geometry = mb.ToMesh();
            gm.Material = Materials.Blue;

            Visual3DModel = gm;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var gm = Visual3DModel as GeometryModel3D;
                gm.Material = gm.Material == Materials.Blue ? Materials.Red : Materials.Blue;
                e.Handled = true;
            }
        }
    }
    public class MinimizedWindow : ContentControl
    {
        
        #region [Controls]
        Grid mainGrid;
        Border contentBorder;
        Button closeButton = null;
        ContentPresenter contentPresenter;
        ItemsControl itemsControl;
        #endregion [Controls]

        #region [Constructors]

        public MinimizedWindow()
        {
            base.DefaultStyleKey = typeof(MinimizedWindow);
            SelectedItem = null;
        }

        #endregion // [Constructors]

        #region [Items Property]

        public ObservableCollection<ControlViewModel> Items
        {
            get { return base.GetValue(ItemsProperty) as ObservableCollection<ControlViewModel>; }
            set { SetValue(ItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Items.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items",
            typeof(ObservableCollection<ControlViewModel>),
            typeof(MinimizedWindow),
            new PropertyMetadata(null, OnItemsChanged));

        private static void OnItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MinimizedWindow minimizeWindow = d as MinimizedWindow;
            ObservableCollection<ControlViewModel> oldValue = e.OldValue as ObservableCollection<ControlViewModel>;
            ObservableCollection<ControlViewModel> newValue = e.NewValue as ObservableCollection<ControlViewModel>;

            if (oldValue != null) oldValue.CollectionChanged -= minimizeWindow.Items_CollectionChanged;
            if (newValue != null) newValue.CollectionChanged += minimizeWindow.Items_CollectionChanged;
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (var item in e.OldItems)
                    if (SelectedItem == item)
                        SelectedItem = null;
            }
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    var collection = itemsControl.FindChildrenInItemsControl<Button>("RestoreButton");
                    foreach (var item in collection)
                    {
                        (item as Button).Click -= restoreButton_Click;
                        (item as Button).Click += restoreButton_Click;
                    }
                }));
            }
        }

        #endregion //[Items Property]

        #region [SelectedProperty]



        public ControlViewModel SelectedItem
        {
            get { return (ControlViewModel)GetValue(SelectedItemProperty); }
            set
            {

                DemoElement3D d = new DemoElement3D();
                if (value == null)
                    IsHidden = true;
                else
                    IsHidden = false;
                SetValue(SelectedItemProperty, value);
            }
        }


        // Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem",
            typeof(ControlViewModel),
            typeof(MinimizedWindow),
            new PropertyMetadata());



        #endregion //[SelectedProperty]

        #region [IsHidden]

        public bool IsHidden
        {
            get { return (bool)GetValue(IsHiddenProperty); }
            set { SetValue(IsHiddenProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsHiden.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsHiddenProperty =
            DependencyProperty.Register("IsHidden", typeof(bool), typeof(MinimizedWindow), new PropertyMetadata());

        #endregion // [IsHidden]

        #region [Direction]

        public WindowDirection Direction
        {
            get { return (WindowDirection)GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Direction.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DirectionProperty =
            DependencyProperty.Register("Direction",
            typeof(WindowDirection),
            typeof(MinimizedWindow),
            new PropertyMetadata(OnDirectionChanged));

        private static void OnDirectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        #endregion [Direction]

        #region [Methods]

        void restoreButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedItem == (sender as FrameworkElement).DataContext as ControlViewModel)
                SelectedItem = null;
            else
                SelectedItem = (sender as FrameworkElement).DataContext as ControlViewModel;
        }

        #endregion [Methods]

        public override void OnApplyTemplate()
        {

            switch (Direction)
            {
                case WindowDirection.Left: { Style = (Style)FindResource("Left"); break; }
                case WindowDirection.Bottom: { Style = (Style)FindResource("Bottom"); break; }
            }
            base.OnApplyTemplate();

            mainGrid = (Grid)this.GetTemplateChild("MainGrid");

            this.FindChild<ItemsControl>("itemsControl", ref itemsControl);

            Dispatcher.BeginInvoke(new Action(() =>
            {
                contentBorder = (Border)this.GetTemplateChild("ContentBorder");
                contentBorder.MouseMove += mainGrid_PreviewMouseMove;

                closeButton = (Button)this.GetTemplateChild("CloseButton");
                closeButton.Click += closeButton_Click;
                this.FindChild<ContentPresenter>("ContentData", ref contentPresenter);

                var collection = itemsControl.FindChildrenInItemsControl<Button>("RestoreButton");
                foreach (var item in collection)
                {
                    (item as Button).Click += restoreButton_Click;
                }
            }));
        }

        void closeButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedItem == null)
                return;
            SelectedItem.ViewState = Enums.ControlViewState.None;
            Items.Remove(SelectedItem);

        }

        void mainGrid_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
          
            if (e.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
            {
                if (SelectedItem != null)
                {
                    var mainWindow = Application.Current.MainWindow as MainWindow;

                    TemplateWindow window = new TemplateWindow();
                    window.SelectedItem = SelectedItem;

                    window.Left = mainWindow.Left + e.GetPosition(mainWindow).X - e.GetPosition(mainGrid).X;
                    window.Top = mainWindow.Top + e.GetPosition(mainWindow).Y + 20;
                    window.Width = ((sender as FrameworkElement).DataContext as ControlViewModel).Width;
                    window.Height = ((sender as FrameworkElement).DataContext as ControlViewModel).Height;
                    mainWindow.ShowWindow(window);
                    Items.Remove(SelectedItem);
                    window.DragMove();
                }
            }
        }
    }
}
