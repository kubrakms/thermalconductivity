﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ThermalConductivity.ViewModels;

using ThermalConductivity.Extension;

namespace ThermalConductivity.Controls
{

    public delegate void DragOverEventHandler(Window sender, ChildWindowDragOverEventArgs e);

    public class TemplateWindow : Window
    {

        #region [Controls]

        Button closeButton = null;
        #endregion //[Controls]

        public event DragOverEventHandler ChildWindowDragOver;

        #region [Constructors]

        public TemplateWindow()
        {

            base.DefaultStyleKey = typeof(TemplateWindow);

            MouseLeftButtonDown += TemplateWindow_MouseLeftButtonDown;
            MouseLeftButtonUp += TemplateWindow_MouseLeftButtonUp;
            LocationChanged += TemplateWindow_LocationChanged;
        }





        #endregion // [Constructors]

        #region [SelectedItem  Property]

        public ControlViewModel SelectedItem
        {
            get { return (ControlViewModel)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem",
            typeof(ControlViewModel),
            typeof(TemplateWindow),
            new PropertyMetadata());


        #endregion // [SelectedItem  Property]

        #region [Methods]

        #region [Overrided ]

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

          Dispatcher.BeginInvoke(new Action(() =>
          {
              this.FindChild<Button>("CloseButton", ref closeButton);
              closeButton.Click += closeButton_Click;

          }));
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.SelectedItem.ViewState = Enums.ControlViewState.None;
            this.Close();
        }

        #endregion // [Overrided ]

        #endregion // [Methods]


        Point mp;



        void TemplateWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                mp = e.GetPosition(sender as Window);
                DragMove();
            }
        }

        void TemplateWindow_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var changedPosition = ChildWindowDragOver;

            if (changedPosition != null)
            {
                changedPosition(this, new ChildWindowDragOverEventArgs(new Point(this.Left + mp.X, this.Top + mp.Y), false));
            }
        }

        void TemplateWindow_LocationChanged(object sender, EventArgs e)
        {
            var changedPosition = ChildWindowDragOver;

            if (changedPosition != null)
            {
                changedPosition(this, new ChildWindowDragOverEventArgs(new Point(this.Left + mp.X, this.Top + mp.Y), true));
            }
        }


    }
}
