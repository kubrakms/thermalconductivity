﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThermalConductivity.Enums
{
    public enum ControlViewState:byte
    {
        None,
        Minimized,
        Window
    }
}
