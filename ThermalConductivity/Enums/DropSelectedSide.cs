﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThermalConductivity.Enums
{
    public enum DropSelectedSide : byte
    {
            None=0,
            Left = 1,
            Right=2,
            Bottom=3,
            Top
    }
}
