﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Contracts.ModelContracts;
using HelixToolkit.Wpf;
using ThermalConductivity.Controls;
using ThermalConductivity.Enums;
using ThermalConductivity.Models;
using ThermalConductivity.ViewModels;
using ThermalConductivity.Views;

using System.Windows.Threading;


namespace ThermalConductivity
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Thumb_DragDelta_1(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            ((sender as FrameworkElement).DataContext as ControlViewModel).Left += e.HorizontalChange;
            ((sender as FrameworkElement).DataContext as ControlViewModel).Top += e.VerticalChange;
        }

        //resize control container
        private void ResizeLeft(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            ((sender as FrameworkElement).DataContext as ControlViewModel).Resize(ResizeOrientation.Left, e.HorizontalChange, e.VerticalChange);
        }

        private void ResizeRight(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            ((sender as FrameworkElement).DataContext as ControlViewModel).Resize(ResizeOrientation.Right, e.HorizontalChange, e.VerticalChange);
        }

        private void ResizeTop(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            ((sender as FrameworkElement).DataContext as ControlViewModel).Resize(ResizeOrientation.Top, e.HorizontalChange, e.VerticalChange);
        }

        private void ResizeBottom(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            ((sender as FrameworkElement).DataContext as ControlViewModel).Resize(ResizeOrientation.Bottom, e.HorizontalChange, e.VerticalChange);
        }




        private void MinimizeRight_Click(object sender, RoutedEventArgs e)
        {
            var window = (sender as FrameworkElement).DataContext as ControlViewModel;
            window.Width = 300;
            window.Height = this.ActualHeight;
            window.Left = this.ActualWidth - 340;
            window.Top = 0;
            (this.DataContext as MainViewModel).OpenedWindows.Remove(window);
            (this.DataContext as MainViewModel).RightMinimizedWindows.Add(window);

        }

        private void MinimizeLeft_Click(object sender, RoutedEventArgs e)
        {
            var window = (sender as FrameworkElement).DataContext as ControlViewModel;
            (this.DataContext as MainViewModel).OpenedWindows.Remove(window);
            (this.DataContext as MainViewModel).LeftMinimizedWindows.Add(window);
        }

        private void MinimizeBottom_Click(object sender, RoutedEventArgs e)
        {
            var window = (sender as FrameworkElement).DataContext as ControlViewModel;
            (this.DataContext as MainViewModel).OpenedWindows.Remove(window);
            (this.DataContext as MainViewModel).BottomMinimizedWindows.Add(window);
        }

        private void RestoreWindow_Click(object sender, RoutedEventArgs e)
        {
            var window = (sender as FrameworkElement).DataContext as ControlViewModel;
            if (!(this.DataContext as MainViewModel).OpenedWindows.Contains(window))
                (this.DataContext as MainViewModel).OpenedWindows.Add(window);
            window.IsHiden = false;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            ((sender as FrameworkElement).DataContext as ControlViewModel).IsHiden = true;
        }

        //public void WindowShow(object sender, RoutedEventArgs e)
        //{
        //    TemplateWindow t = new TemplateWindow();
        //    t.SelectedItem = PropertyGridViewModel.Instance;
        //    t.ChildWindowDragOver += t_ChildWindowDragOver;
        //    t.Show();
        //}



        void t_ChildWindowDragOver(Window sender, ChildWindowDragOverEventArgs e)
        {
            //get side magic


            Rect LeftSide = new Rect(this.Left, this.Top, 300, this.ActualHeight / 4 * 3);
            Rect RightSide = new Rect(this.Left + this.ActualWidth - 300, this.Top, 300, this.ActualHeight / 4 * 3);
            Rect BottomSide = new Rect(Left, Top + ActualHeight / 4 * 3, ActualWidth, ActualHeight / 4);

            DropSelectedSide result = DropSelectedSide.None;

            if (LeftSide.Contains(e.Position))
                result = DropSelectedSide.Left;

            if (RightSide.Contains(e.Position))
                result = DropSelectedSide.Right;

            if (BottomSide.Contains(e.Position))
                result = DropSelectedSide.Bottom;

            if (!e.IsLeftMouseButtonDown)
            {
                switch (result)
                {
                    case DropSelectedSide.Left:
                        {
                            (DataContext as MainViewModel).LeftMinimizedWindows.Add((sender as TemplateWindow).SelectedItem);
                            sender.Close();
                            break;
                        }
                    case DropSelectedSide.Right:
                        {
                            (DataContext as MainViewModel).RightMinimizedWindows.Add((sender as TemplateWindow).SelectedItem);
                            sender.Close();
                            break;
                        }
                    case DropSelectedSide.Bottom:
                        {
                            (DataContext as MainViewModel).BottomMinimizedWindows.Add((sender as TemplateWindow).SelectedItem);
                            sender.Close();
                            break;
                        }
                }
                (sender as TemplateWindow).SelectedItem.ViewState = ControlViewState.Minimized;

                result = DropSelectedSide.None;
            }
            (DataContext as MainViewModel).DropControl.SelectedSide = result;
        }

        private void ViewOutput_Click(object sender, RoutedEventArgs e)
        {
            ShowWindow(OutputViewModelMain.Instance);
        }

        private void ViewPropertyGrid_Click(object sender, RoutedEventArgs e)
        {
            ShowWindow(PropertyGridViewModel.Instance);
        }

        private void ViewToolBox_Click(object sender, RoutedEventArgs e)
        {
            var control = ToolBoxViewModel.Instance;
            ShowWindow(control);
        }


        byte r = 250;
        byte g, b;
        bool isLast;

        private void ColorScale_Click(object sender, RoutedEventArgs e)
        {
            r = 250;
            g=0;b=0;
            isLast = false;
            ColorScaleViewModel.Instance.ColorDictionary.Clear();
            SomeMethod();
            ShowWindow(ColorScaleViewModel.Instance);
        }

        private void CuttingPlanes_Click(object sender, RoutedEventArgs e)
        {
            ShowWindow(CubeCuttingPlanesViewModel.Instance);
        }


        private void SomeMethod()
        {
            int temperature = 80;

            while (g < 250 && !isLast)
            {
                g += 25;
                ColorScaleViewModel.Instance.ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                temperature -= 2;

            }
            while (r > 0)
            {
                ColorScaleViewModel.Instance.ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                r -= 25;
                temperature -= 2;

            }
            while (b < 250)
            {
                ColorScaleViewModel.Instance.ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                b += 25;
                temperature -= 2;
            }
            while (g > 0)
            {
                isLast = true;
                ColorScaleViewModel.Instance.ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                g -= 25;
                temperature -= 2;

            }
        }

        public void ShowWindow(TemplateWindow window)
        {
            window.SelectedItem.IsHiden = false;
            window.ChildWindowDragOver += t_ChildWindowDragOver;
            //  window.ShowInTaskbar = false;
            window.Show();
        }

        public void ShowWindow(ControlViewModel control)
        {
            if (control.ViewState == ControlViewState.Minimized)
            {
                control.IsHiden = false;
                return;
            }
            if (control.ViewState == ControlViewState.None)
            {
                control.ViewState = ControlViewState.Window;
                TemplateWindow window = new TemplateWindow();
                window.SelectedItem = control;
                window.SelectedItem.IsHiden = false;
                window.ChildWindowDragOver += t_ChildWindowDragOver;
                //window.ShowInTaskbar = false;
                window.Show();
            }
        }

        private void OnMainWindowClosed(object sender, EventArgs e)
        {
            foreach (Window item in Application.Current.Windows)
                item.Close();
        }

        private void Option_Click(object sender, RoutedEventArgs e)
        {

        }

        private void OnMainWindowLoaded(object sender, RoutedEventArgs e)
        {
            //  (this.DataContext as MainViewModel).OnPropertyChanged("ProjectionControl");
        }

        private void OnItemDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ModelVisual3D)))
            {
                var tempObject = e.Data.GetData(typeof(ModelVisual3D));

                if (tempObject is ILightContract)
                {
                    //(this.DataContext as MainViewModel).ProjectionControl.Objects3D.Add(tempObject as Visual3D);
                }

                if (tempObject is IObjectContract)
                {
                    PropertyGridViewModel.Instance.SelectedObject = tempObject;
                    if (tempObject is CubeModel)
                    {
                        (tempObject as CubeModel).TemperatureMatrixChanged -= CubeCuttingPlanesViewModel.Instance.TemperatureMatrixChanged;
                        (tempObject as CubeModel).TemperatureMatrixChanged += CubeCuttingPlanesViewModel.Instance.TemperatureMatrixChanged;
                        ArrowVisual3D arrow = new ArrowVisual3D(){ Point1 = new Point3D(5, -5, 5),  Point2 = new Point3D(3, -3, 3), Diameter = 0.2, Fill = new SolidColorBrush(Colors.Red), HeadLength=1 };
                        (this.DataContext as MainViewModel).ProjectionControl.Objects3D.Add(arrow);
                    }
                    if (tempObject is BeCylinderModel)
                    {
                     
                        ArrowVisual3D arrow3 = new ArrowVisual3D() { Point1 = new Point3D(0, 0, -8), Point2 = new Point3D(0, 0, -2), Diameter = 1, Fill = new SolidColorBrush(Colors.Red), HeadLength = 2 };
                        
                      
                        (this.DataContext as MainViewModel).ProjectionControl.Objects3D.Add(arrow3);
                       
                    }
                }


                (this.DataContext as MainViewModel).ProjectionControl.Objects3D.Add(tempObject as Visual3D);
            }
        }

        void MainWindow_TemperatureMatrixChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void File_Click(object sender, RoutedEventArgs e)
        {
            // ((this.DataContext as MainViewModel).ProjectionControl.Objects3D[0] as CubeModel).Size++;
        }

        

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        DispatcherTimer t = new DispatcherTimer();
        private void Timer_Start(object sender, RoutedEventArgs e)
        {
            var model = (PropertyGridViewModel.Instance.SelectedObject as IObjectContract);
            if (model == null) return;
            model.Time = 1;

            t.Interval = TimeSpan.FromMilliseconds(100);
            t.Tick += t_Tick;
            t.Start();
        }

        void t_Tick(object sender, EventArgs e)
        {
            var model = (PropertyGridViewModel.Instance.SelectedObject as IObjectContract);
            if (model == null)
            {
                t.Stop();
                return;
            }
            if (model.Time < 110)
                model.Time++;
            else
            {
                (sender as DispatcherTimer).Stop();
            }
            //PropertyGridViewModel.Instance.SelectedObject = model;
        }

        private void ClearViewPort_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as MainViewModel).ProjectionControl.Objects3D.Clear();
            PropertyGridViewModel.Instance.SelectedObject = null;
           
        }

        private void Timer_Stop(object sender, RoutedEventArgs e)
        {
            var model = (PropertyGridViewModel.Instance.SelectedObject as IObjectContract);
            if (model == null) return;
            t.Stop();
        }

       
       

       
    }
}
