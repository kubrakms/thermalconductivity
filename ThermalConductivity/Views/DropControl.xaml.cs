﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ThermalConductivity.Views
{
    /// <summary>
    /// Interaction logic for DropControl.xaml
    /// </summary>
    public partial class DropControl : UserControl
    {
        public DropControl()
        {
            InitializeComponent();
        }

        private void Button_DragEnter_1(object sender, DragEventArgs e)
        {

        }

        private void Button_Drop_1(object sender, DragEventArgs e)
        {

        }

        private void Image_Drop_1(object sender, DragEventArgs e)
        {

        }
    }
}
