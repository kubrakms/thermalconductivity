﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThermalConductivity.Views
{
    public enum WindowDirection
    {
        Left,
        Top,
        Right,
        Bottom
    }
}
