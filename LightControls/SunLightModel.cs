﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts.ModelContracts;
using HelixToolkit.Wpf;


namespace ThermalConductivity.Models
{
    public class SunLightModel : SunLight, ILightContract
    {
        public string Description
        {
            get;
            set;
        }

        public string Source
        {
            get;
            set;
        }

        public SunLightModel()
        {
            Description = "SunLight";
        }

       

        
    }
}
