﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;


namespace Contracts.ModelContracts
{
    public interface IObjectContract : IContactBase
    {
        int Time { get; set; }

        //Point3D PlanPoint { get; set; }
        //Vector3D PlanVector { get; set; }
        //List<double> PlaneCuttingValues { get; set; }
    }
}
