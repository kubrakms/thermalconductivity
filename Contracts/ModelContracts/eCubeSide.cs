﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contracts.ModelContracts
{
    public enum eCubeSide : byte
    {
        Left,
        Right,
        Back,
        Front,
        Top,
        Bottom
    }
}
