﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ToolBox.Extension
{
    public static class FrameworkElementExtension
    {
        public static void FindChild<T>(this FrameworkElement start, string name, ref T result) where T : FrameworkElement
        {
            if (start == null) return;
            if (result != null) return;
            if (start.Name == name)
            {
                result = start as T;
                return;
            }
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(start); i++)
                FindChild(VisualTreeHelper.GetChild(start, i) as FrameworkElement, name, ref result);
            if (result != null) return;
            //if (VisualTreeHelper.GetChildrenCount(start) == 0)
            //{

            //}
            if (start is ItemsControl)
            {
                foreach (var item in (start as ItemsControl).Items)
                {
                    FindChild(item as FrameworkElement, name, ref result);
                    if (result != null) return;
                }
            }
        }

        public static IEnumerable<T> FindChildrenInItemsControl<T>(this ItemsControl start, string name) where T : FrameworkElement
        {
            List<T> resultList = new List<T>();
            start.UpdateLayout();
            foreach (var item in start.Items)
            {
                start.ItemContainerGenerator.ItemsChanged += ItemContainerGenerator_ItemsChanged;
                start.ItemContainerGenerator.StatusChanged += ItemContainerGenerator_StatusChanged;
                var container = start.ItemContainerGenerator.ContainerFromItem(item) as FrameworkElement;
                T res = null;

                //if (container is ContentPresenter)
                //    res = (T)(container as ContentPresenter).ContentTemplate.LoadContent();
                //else
                    res = start.ItemTemplate.FindName(name, container) as T;
               
               
                if (res != null)
                    resultList.Add(res);
            }
            return resultList;
        }

        static void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
        {
            
        }

        static void ItemContainerGenerator_ItemsChanged(object sender, System.Windows.Controls.Primitives.ItemsChangedEventArgs e)
        {
           
        }
    }
}
