﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Contracts.ModelContracts;
using HelixToolkit.Wpf;
using ThermalConductivity.Models;
using ToolBox.Extension;

namespace ThermalConductivity.Controls
{
    public class ToolBox : ContentControl
    {
        #region [Controls]
        Grid itemGrid = null;
        #endregion // [Controls]

        #region [Constructors]



        public ToolBox()
        {

            instance = this;
            base.DefaultStyleKey = typeof(ToolBox);

            //Items = new ObservableCollection<IContactBase>();
            //Items.CollectionChanged += Items_CollectionChanged;

            Objects = new ObservableCollection<IObjectContract>();
            Lights = new ObservableCollection<ILightContract>();
            Heaters = new ObservableCollection<IHeaterContract>();
        }



        #endregion // [Constructors]

        #region [Fields]
        private static ToolBox instance;
        #endregion // [Fields]

        #region [Properties]

        #region [Items Property]

        public ObservableCollection<IContactBase> Items
        {
            get { return (ObservableCollection<IContactBase>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Items.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items",
                                        typeof(ObservableCollection<IContactBase>),
                                        typeof(ToolBox),
                                        new PropertyMetadata(CollectionSet));


        private static void CollectionSet(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((d as ToolBox).Items == null)
                return;

            (d as ToolBox).Items_CollectionChanged(null,new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add,(d as ToolBox).Items));

            (d as ToolBox).Items.CollectionChanged += (d as ToolBox).Items_CollectionChanged;
        }

        void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            foreach (IContactBase item in e.NewItems)
            {
                if (item is IObjectContract)
                    Objects.Add(item as IObjectContract);

                if (item is ILightContract)
                    Lights.Add(item as ILightContract);

                if (item is IHeaterContract)
                    Heaters.Add(item as IHeaterContract);
            }
        }

        #endregion // [Items Property]

        #region [Objects Property]

        public ObservableCollection<IObjectContract> Objects
        {
            get { return (ObservableCollection<IObjectContract>)GetValue(ObjectsProperty); }
            set { SetValue(ObjectsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Objects.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ObjectsProperty =
            DependencyProperty.Register("Objects",
            typeof(ObservableCollection<IObjectContract>),
            typeof(ToolBox),
            new PropertyMetadata());

        #endregion // [Objects Property]

        #region [Lights Property]

        public ObservableCollection<ILightContract> Lights
        {
            get { return (ObservableCollection<ILightContract>)GetValue(LightsProperty); }
            set { SetValue(LightsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Lights.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LightsProperty =
            DependencyProperty.Register("Lights",
                                        typeof(ObservableCollection<ILightContract>),
                                        typeof(ToolBox),
                                        new PropertyMetadata());

        #endregion // [Lights Property]

        #region [Heater Property]

        public ObservableCollection<IHeaterContract> Heaters
        {
            get { return (ObservableCollection<IHeaterContract>)GetValue(HeatersProperty); }
            set { SetValue(HeatersProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Heaters.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeatersProperty =
            DependencyProperty.Register("Heaters",
                                        typeof(ObservableCollection<IHeaterContract>),
                                        typeof(ToolBox),
                                        new PropertyMetadata());



        #endregion // [Heater Property]

        #endregion //[Properties]

        #region [Methods]

        #endregion // [Methods]

        #region [Overrides]

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Dispatcher.BeginInvoke(new Action(() =>
            {
                var listbox = (StackPanel)this.GetTemplateChild("MainStackPanel");

                foreach (var item in listbox.Children)
                {
                    var ic = (item as Expander).Content as ItemsControl;
                    var items = ic.FindChildrenInItemsControl<Grid>("ItemGrid");
                    foreach (Grid gr in items)
                    {
                        gr.PreviewMouseLeftButtonDown -= grid_PreviewMouseLeftButtonDown;
                        gr.PreviewMouseLeftButtonDown += grid_PreviewMouseLeftButtonDown;
                    }
                }
            }));
        }

        void grid_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var dc = new DataObject(typeof(ModelVisual3D), (sender as Grid).DataContext);
            DragDrop.DoDragDrop(sender as Grid, dc, DragDropEffects.Move);
        }

        #endregion // [Overrides]

    }
}
