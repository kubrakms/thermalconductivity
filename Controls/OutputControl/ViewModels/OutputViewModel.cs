﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThermalConductivity.Controls.ViewModels
{
    public class OutputViewModel : INotifyPropertyChanged
    {
        #region [INotifyPropertyChanged implementation]

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged != null)
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion // [INotifyPropertyChanged implementation]

        #region [Fields]

        private string messages;

        #endregion //[Fields]

        #region [Properties]

        public string Messages
        {
            get { return messages; }
            set 
            {
                messages +=value+ "\r\n" ;
                OnPropertyChanged("Messages");
            }
        }

        #endregion // [Properties]

        #region [Constructors]

        public OutputViewModel()
        {
            Messages = @"a123123123Background/context for this question: I have a WPF desktop application. It uses LINQ to SQL to connect to its SQL database, and it displays its data in WPF Datagrids. It was working fairly well, but performance was a problem because LINQ can be deadly slow, so I have been switching my logic and UI ";
            Messages = "a123123123";
            Messages = "a123123123";
            Messages = "a123123123";
            Messages = "a123123123";
            Messages = "a123123123";
            Messages = "a123123123";
            Messages = "a123123123";
        }

        #endregion // [Constructors]

    }
}
