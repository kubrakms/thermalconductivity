﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ThermalConductivity.Controls.ViewModels;

namespace ThermalConductivity.Controls
{
    /// <summary>
    /// Interaction logic for OutputControl.xaml
    /// </summary>
    public partial class OutputControl : UserControl
    {
        public OutputControl()
        {
            DataContext = new OutputViewModel();
            InitializeComponent();
        }

        public string History
        {
            get { return (string)GetValue(HistoryProperty); }
            set { SetValue(HistoryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for History.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HistoryProperty =
            DependencyProperty.Register("History", typeof(string), typeof(OutputControl),new PropertyMetadata(null, OnHistoryAdded));

        private static void OnHistoryAdded(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((d as FrameworkElement).DataContext as OutputViewModel).Messages=(e.NewValue.ToString());
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {

        }

        private void Window_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragDrop.DoDragDrop(this, this, DragDropEffects.Move);
        }

        private void Window_MouseMove_1(object sender, MouseEventArgs e)
        {

        }

    }
}
