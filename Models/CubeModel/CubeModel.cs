﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Contracts.ModelContracts;
using HelixToolkit.Wpf;
using ThermalConductivity.Models;
using Appliques;
using System.Windows;
using System.ComponentModel;
using MIConvexHull;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Drawing;
using System.Collections.ObjectModel;


namespace ThermalConductivity.Models
{
    public class CubeModel : MeshVisual3D, IObjectContract, IContactBase,INotifyPropertyChanged
    {
        public event EventHandler TemperatureMatrixChanged;

        #region [Constants]

        #endregion // [Constants]

        #region [Constructors]

        public CubeModel()
        {

            ColorDictionary = new ObservableCollection<KeyValuePair<int, System.Drawing.Color>>();
            SomeMethod();
            planPoint = new Point3D(0, 0, 1);
            planVector = new Vector3D(0, 0, -1);
            Description = "Cube";
            Size = 5;

            UpdateVisuals();
        }

        #endregion // [Constructors]

        #region [Fields]


        private Point3D planPoint;
        private Vector3D planVector;
        private double width;
        private double height;
        private double length;
        private double size;
        private double xCutting;
        private double yCutting;
        private double zCutting;


        #endregion // [Fields]


        #region [Properties]


        public double Size
        {
            get { return size; }
            set
            {
                Width = value;
                Height = value;
                Length = value;
                size = value;
                UpdateVisuals();
            }
        }

        private double Width
        {
            get { return width; }
            set { width = value; }
        }

        private double Height
        {
            get { return height; }
            set { height = value; }
        }

        private double Length
        {
            get { return length; }
            set { length = value; }
        }

        public double XCutting
        {
            get { return xCutting; }
            set
            {
                xCutting = value;

                Width = value;
                UpdateVisuals();
            }
        }

        public double YCutting
        {
            get { return yCutting; }
            set
            {
                yCutting = value;

                Height = value;
                UpdateVisuals();
            }
        }

        public double ZCutting
        {
            get { return zCutting; }
            set
            {
                zCutting = value;

                Length = value;
                UpdateVisuals();
            }
        }

        public string Description
        {
            get;
            set;
        }

        private int time = 1;

        public int Time
        {
            get { return time; }
            set
            {
                time = value;
                OnPropertyChanged("Time");
                UpdateVisuals();
                TemperatureMatrixChanged(this, new EventArgs());
            }
        }


        public double[, ,] TemperatureMatrix
        {
            get;
            set;
        }

        ObservableCollection<KeyValuePair<int, System.Drawing.Color>> colorDictionary;
        public ObservableCollection<KeyValuePair<int, System.Drawing.Color>> ColorDictionary
        {
            get { return colorDictionary; }
            set
            {
                colorDictionary = value;

            }
        }

        #endregion // [Properties]

        #region [Dependencies Properties]





        #endregion // [Dependencies Properties]

        #region [Methods]

        #region [Public]

        #endregion // [Public]

        #region [Private and Protected]



        private void UpdateVisuals()
        {
            TemperatureMatrix = CalculateTemperatureMatrix(new Point3D(0, 0, 0), Time);

            this.Children.Clear();
            this.VertexRadius = 0.5;
            this.VertexMaterial = Materials.Gold;
            this.EdgeDiameter = 0.5;

            var up = new Vector3D(0, 0, 1);
            var right = new Vector3D(0, 1, 0);
            var front = Vector3D.CrossProduct(right, up);

            MeshVisual3D tempModel = new MeshVisual3D();// new CubeVisual3D() { Width = Size, Height = Size, Length = Size };


            tempModel.Children.Add(this.CreateFace(Side.Front, TemperatureMatrix));
            tempModel.Children.Add(this.CreateFace(Side.End, TemperatureMatrix));
            tempModel.Children.Add(this.CreateFace(Side.Left, TemperatureMatrix));
            tempModel.Children.Add(this.CreateFace(Side.Right, TemperatureMatrix));
            tempModel.Children.Add(this.CreateFace(Side.Top, TemperatureMatrix));
            tempModel.Children.Add(this.CreateFace(Side.Bottom, TemperatureMatrix));

            this.Children.Add(tempModel);

        }

        private ModelUIElement3D CreateFace(Side side, double[, ,] tempMatrix)
        {

            var builder = new MeshBuilder(false, true);
            switch (side)
            {
                case Side.End:
                    {
                        builder.AddCubeFace(new Point3D(0, 0, 0), new Vector3D(-1, 0, 0), new Vector3D(0, 0, 1), this.Length, this.Width, this.Height);
                        break;
                    }
                case Side.Front:
                    {
                        builder.AddCubeFace(new Point3D(0, 0, 0), new Vector3D(1, 0, 0), new Vector3D(0, 0, -1), this.Length, this.Width, this.Height);
                        break;
                    }
                case Side.Left:
                    {
                        builder.AddCubeFace(new Point3D(0, 0, 0), new Vector3D(0, -1, 0), new Vector3D(0, 0, 1), this.Width, this.Length, this.Height);
                        break;
                    }
                case Side.Right:
                    {
                        builder.AddCubeFace(new Point3D(0, 0, 0), new Vector3D(0, 1, 0), new Vector3D(0, 0, -1), this.Width, this.Length, this.Height);
                        break;
                    }
                case Side.Top:
                    {
                        builder.AddCubeFace(new Point3D(0, 0, 0), new Vector3D(0, 0, 1), new Vector3D(0, -1, 0), this.Height, this.Length, this.Width);
                        break;
                    }
                case Side.Bottom:
                    {
                        builder.AddCubeFace(new Point3D(0, 0, 0), new Vector3D(0, 0, -1), new Vector3D(0, 1, 0), this.Height, this.Length, this.Width);
                        break;
                    }
            }

            var geometry = builder.ToMesh();
            Bitmap bitmap = GetBitmapForSide(side);
            var bitmapSoure = BitmapHelper.LoadBitmap(bitmap);
            var model = new GeometryModel3D
            {
                Geometry = geometry,
                Material = MaterialHelper.CreateMaterial(new ImageBrush(bitmapSoure)),
                BackMaterial = MaterialHelper.CreateMaterial(new ImageBrush(bitmapSoure))
            };
            var element = new ModelUIElement3D { Model = model };
            return element;

        }

        private double[, ,] CalculateTemperatureMatrix(Point3D heatPoint, int time)
        {
            double[, ,] temperatureMatrix = new double[50, 50, 50];
            for (int x = 0; x < Size * 10; x++)
            {
                for (int y = 0; y < Size * 10; y++)
                {
                    for (int z = 0; z < Size * 10; z++)
                    {
                        temperatureMatrix[z, y, x] = (time - Math.Abs(Math.Sqrt(Math.Pow(x - heatPoint.X, 2) + Math.Pow(y - heatPoint.Y, 2) + Math.Pow(z - heatPoint.Z, 2))) * 2);
                    }
                }
            }

            return temperatureMatrix;

        }

        public Bitmap GetBitmapForSide(Side side)
        {
            int lenghtCount = 50;
            Bitmap b = new Bitmap(lenghtCount, lenghtCount);

            switch (side)
            {
                //+
                case Side.Left:
                    {
                        for (int i = 0; i < lenghtCount; i++)
                            for (int j = 0; j < lenghtCount; j++)
                                b.SetPixel(lenghtCount - 1 - j, i, GetColorByTemperature((int)TemperatureMatrix[0, i, j]));
                        break;
                    }

                //+
                case Side.Top:
                    {
                        for (int i = 0; i < lenghtCount; i++)
                            for (int j = 0; j < lenghtCount; j++)
                                b.SetPixel(i, j, GetColorByTemperature((int)TemperatureMatrix[i, j, 0]));
                        break;
                    }
                // ++
                case Side.Front:
                    {
                       
                        for (int i = 0; i < lenghtCount; i++)
                            for (int j = 0; j < lenghtCount; j++)
                                b.SetPixel(lenghtCount - 1 - i, lenghtCount-1-j, GetColorByTemperature((int)TemperatureMatrix[i, 0, j]));
                        break;
                    }

                case Side.Right:
                    {

                        for (int i = 0; i < lenghtCount; i++)
                            for (int j = 0; j < lenghtCount; j++)
                                b.SetPixel(lenghtCount - 1 - i, lenghtCount - 1 - j, GetColorByTemperature((int)TemperatureMatrix[(int)(Width * 10) - 1, i, j]));

                        break;
                    }
                case Side.End:
                    {

                        for (int i = 0; i < lenghtCount; i++)
                            for (int j = 0; j < lenghtCount; j++)
                                b.SetPixel(lenghtCount - 1 - i, j, GetColorByTemperature((int)TemperatureMatrix[i, (int)(Height * 10) - 1, j]));

                        break;
                    }


                case Side.Bottom:
                    {
                        for (int i = 0; i < lenghtCount; i++)
                            for (int j = 0; j < lenghtCount; j++)
                                b.SetPixel(i, lenghtCount - 1 - j, GetColorByTemperature((int)TemperatureMatrix[i, j, (int)(Length * 10) - 1]));

                        break;
                    }
            }
            return b;
        }


      
        private System.Drawing.Color GetColorByTemperature(double temperature)
        {
            for (int i = 0; i < ColorDictionary.Count; i++)
            {
                if (temperature < 2)
                    return System.Drawing.Color.Blue;
                if (temperature >= 80)
                    return System.Drawing.Color.Red;
                if (temperature >= ColorDictionary[i].Key)
                    return ColorDictionary[i - 1].Value;
            }
            return System.Drawing.Color.Black;
        }


        byte r = 250;
        byte g, b;
        bool isLast;
        private void SomeMethod()
        {
            int temperature = 80;

            while (g < 250 && !isLast)
            {
                g += 25;
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                temperature -= 2;

            }
            while (r > 0)
            {
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                r -= 25;
                temperature -= 2;

            }
            while (b < 250)
            {
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                b += 25;
                temperature -= 2;
            }
            while (g > 0)
            {
                isLast = true;
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                g -= 25;
                temperature -= 2;

            }
        }

        #endregion //[Private and Protected]

        #endregion // [Methods]

        #region [Stuff]

        //private ModelUIElement3D CreateImageFace(Vector3D normal, Vector3D up, BitmapImage b)
        //{

        //    //var grid = new Grid { Width = 20, Height = 20, Background = Brushes.Aqua };

        //    //grid.Arrange(new Rect(new Point(0, 0), new Size(20, 20)));

        //    //var bmp = new RenderTargetBitmap((int)grid.Width, (int)grid.Height, 96, 96, PixelFormats.Default);
        //    //bmp.Render(grid);

        //    Material material = MaterialHelper.CreateMaterial(new ImageBrush(b));

        //    double a = this.Size;

        //    var builder = new MeshBuilder(false, true);
        //    builder.AddCubeFace(new Point3D(0, 0, 0), normal, up, a, a, a);
        //    var geometry = builder.ToMesh();
        //    geometry.Freeze();

        //    var model = new GeometryModel3D { Geometry = geometry, Material = material };
        //    var element = new ModelUIElement3D { Model = model };
        //    return element;
        //}

        //private Visual3D CreatePoligon(List<Point3D> points, Brush b)
        //{
        //    Polygon3D p = new Polygon3D(points);

        //    List<Point> listPoints2D = new List<Point>();
        //    List<Point3D> listPoints3D = new List<Point3D>(points);
        //    foreach (var item in points)
        //        listPoints2D.Add(new Point(item.X, item.Y));

        //    SortPoligonContour(ref listPoints2D, ref listPoints3D);

        //    p = new Polygon3D(listPoints3D);
        //    Polygon pres = p.Flatten();

        //    var triangles = CuttingEarsTriangulator.Triangulate(pres.Points);
        //    var normals = MeshGeometryHelper.CalculateNormals(listPoints3D, triangles);


        //    var builder = new MeshBuilder(false, false);

        //    builder.Append(listPoints3D, triangles);



        //    var geometry = builder.ToMesh();

        //    var model = new GeometryModel3D { Geometry = geometry, Material = MaterialHelper.CreateMaterial(b), BackMaterial = Materials.Gold };
        //    var element = new ModelUIElement3D { Model = model };
        //    return element;
        //}



        //private void SortPoligonContour(ref List<Point> list2D, ref List<Point3D> list3D)
        //{
        //    //initialize collections
        //    List<Point> processedList = new List<Point>();
        //    List<Point> notProcessedList = new List<Point>();
        //    for (int i = 0; i < list2D.Count; i++)
        //        notProcessedList.Add(list2D[i]);

        //    //get left top node
        //    Point currentPoint = new Point();
        //    foreach (var item in notProcessedList)
        //        if (item.X <= currentPoint.X && item.Y <= currentPoint.Y)
        //            currentPoint = item;

        //    processedList.Add(currentPoint);
        //    notProcessedList.Remove(currentPoint);
        //    while (notProcessedList.Count > 0)
        //    {
        //        currentPoint = GetNextPoint(currentPoint, notProcessedList);
        //        processedList.Add(currentPoint);
        //    }


        //    List<Point3D> processed3DList = new List<Point3D>();


        //    for (int i = 0; i < processedList.Count; i++)
        //    {
        //        for (int j = 0; j < list2D.Count; j++)
        //        {
        //            if (list2D[j].X == processedList[i].X && list2D[j].Y == processedList[i].Y)
        //                processed3DList.Add(list3D[j]);
        //        }

        //    }

        //    //  list2D = processedList;
        //    list3D = processed3DList;


        //}

        ////private Point GetNextPoint(Point currentPoint, List<Point> notpProcesseList)
        ////{
        ////    Point? nextCandidate = null;



        ////    #region  find to up form currentPoint
        ////    foreach (var item in notpProcesseList)
        ////    {
        ////        if (item.X <= currentPoint.X && item.Y > currentPoint.Y)
        ////        {
        ////            if (nextCandidate == null)
        ////                nextCandidate = item;
        ////            if (item.Y < nextCandidate.Value.Y)
        ////                nextCandidate = item;
        ////        }
        ////    }

        ////    if (nextCandidate != null)
        ////    {
        ////        notpProcesseList.Remove(nextCandidate.Value);
        ////        return nextCandidate.Value;
        ////    }
        ////    #endregion // find to up form currentPoint

        ////    #region  find to right form currentPoint

        ////    foreach (var item in notpProcesseList)
        ////    {
        ////        if (item.Y >= currentPoint.Y && item.X > currentPoint.X)
        ////        {
        ////            if (nextCandidate == null)
        ////                nextCandidate = item;
        ////            if (item.X < nextCandidate.Value.X)
        ////                nextCandidate = item;
        ////        }
        ////    }

        ////    if (nextCandidate != null)
        ////    {
        ////        notpProcesseList.Remove(nextCandidate.Value);
        ////        return nextCandidate.Value;
        ////    }
        ////    #endregion // find to right form currentPoint

        ////    #region  find to down form currentPoint
        ////    foreach (var item in notpProcesseList)
        ////    {
        ////        if (item.X >= currentPoint.X && item.Y < currentPoint.Y)
        ////        {
        ////            if (nextCandidate == null)
        ////                nextCandidate = item;
        ////            if (item.Y > nextCandidate.Value.Y)
        ////                nextCandidate = item;
        ////        }
        ////    }

        ////    if (nextCandidate != null)
        ////    {
        ////        notpProcesseList.Remove(nextCandidate.Value);
        ////        return nextCandidate.Value;
        ////    }
        ////    #endregion // find to down form currentPoint

        ////    #region  find to left form currentPoint

        ////    foreach (var item in notpProcesseList)
        ////    {
        ////        if (item.Y <= currentPoint.Y && item.X < currentPoint.X)
        ////        {
        ////            if (nextCandidate == null)
        ////                nextCandidate = item;
        ////            if (item.X > nextCandidate.Value.X)
        ////                nextCandidate = item;
        ////        }
        ////    }

        ////    if (nextCandidate != null)
        ////    {
        ////        notpProcesseList.Remove(nextCandidate.Value);
        ////        return nextCandidate.Value;
        ////    }
        ////    #endregion // find to left form currentPoint

        ////    throw new Exception();
        ////}

        //private void GauseSLAU(int nb, int ne, double[,] a, double[] f, ref double[] x)
        //{
        //    double s, eps;
        //    int k, kr = 1, l;
        //    eps = 1E-11;
        //    for (k = 0; k < ne - 1; k++)
        //    {
        //        if (Math.Abs(a[k, k]) < eps)
        //        {
        //            l = k - 1;
        //            do
        //            {
        //                l = l + 1;
        //                k = 1;
        //                if (Math.Abs(a[l, k]) > eps)
        //                    kr = 0;
        //                if (l == ne)
        //                    kr = 0;
        //            } while (kr == 0);

        //            for (int j = nb; j < ne; j++)
        //            {
        //                s = a[k, j]; a[k, j] = a[l, j]; a[l, j] = s;
        //            }
        //            s = f[k]; f[k] = f[l]; f[l] = s;
        //        }

        //        for (int i = k + 1; i < ne; i++)
        //        {
        //            s = a[i, k] / a[k, k];
        //            a[i, k] = 0;
        //            for (int j = k + 1; j < ne; j++)
        //                a[i, j] = a[i, j] - s * a[k, j];
        //            f[i] = f[i] - s * f[k];
        //        }
        //    }
        //    x[ne] = f[ne] / a[ne, ne];
        //    for (int i = ne - 1; i > nb; i--)
        //    {
        //        s = 0;
        //        for (k = i + 1; k < ne; k++)
        //        {
        //            s = s + a[i, k] * x[k];
        //            x[i] = (f[i] - s) / a[i, i];
        //        }
        //    }
        //}

        #endregion // [Stuff]


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            if(propertyChanged!=null)
                propertyChanged(this,new PropertyChangedEventArgs(propertyName));
        }
    }
    public enum Side
    {
        Front,
        End,
        Left,
        Right,
        Top,
        Bottom
    }
}
