﻿using Contracts.ModelContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using Petzold.Media3D;
using System.Drawing;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ThermalConductivity.Models
{
    public class BeCylinderModel : ModelVisual3D, IObjectContract, INotifyPropertyChanged
    {

        #region [Constructors]

        public BeCylinderModel()
        {
            CreateData();
            ColorDictionary = new ObservableCollection<KeyValuePair<int, System.Drawing.Color>>();


            //  Run();

            planVector = new Vector3D(0, 0, -1);
            planPoint = new Point3D(0, 0, 20);


            PlaneCuttingValues = new List<double>();

            Description = "BeCylinder";
            Time = 1;
            UpdateVisuals();
        }

        #endregion // [Constructors]

        #region [Fields]

        public event PropertyChangedEventHandler PropertyChanged;
        Point3D planPoint;
        Vector3D planVector;

        private double length = 20;
        private double diametr = 5;
        private double innerDiametr = 2;

        #endregion //[Fields]

        #region [DependencyProperties]














        #endregion // [DependencyProperties]

        #region [Properties]

        public double Length
        {
            get { return length; }
            set
            {
                length = value;
                UpdateVisuals();
            }
        }

        public double Diametr
        {
            get { return diametr; }
            set
            {
                diametr = value;
                UpdateVisuals();
            }
        }

        public double InnerDiametr
        {
            get { return innerDiametr; }
            set
            {
                innerDiametr = value;
                UpdateVisuals();
            }
        }

        private System.Windows.Media.Brush cuttingBrush = new ImageBrush();//new BitmapImage(new Uri(@"f:\Projects\thermalconductivity\ThermalConductivity\bin\Debug\gradient.png", UriKind.Absolute)));
        public System.Windows.Media.Brush CuttingBrush
        {
            get { return cuttingBrush; }
            set
            {

                cuttingBrush = value;
                UpdateVisuals();
            }
        }

        public string Description
        {
            get;
            set;
        }

        public string Source
        {
            get;
            set;
        }

        public Point3D PlanPoint
        {
            get { return planPoint; }
            set
            {
                planPoint = value;
                UpdateTexture();
                UpdateVisuals();
            }
        }

        public Vector3D PlanVector
        {
            get { return planVector; }
            set
            {
                planVector = value;
                UpdateVisuals();
            }
        }

        public List<double> PlaneCuttingValues
        {
            get;
            set;
        }

        private int time;

        public int Time
        {
            get { return time; }
            set
            {
                if (value > 0 && value < 120)
                {
                    time = value;
                    OnPropertyChanged("Time");
                    UpdateTexture();
                }
            }
        }

        ObservableCollection<KeyValuePair<int, System.Drawing.Color>> colorDictionary;
        public ObservableCollection<KeyValuePair<int, System.Drawing.Color>> ColorDictionary
        {
            get { return colorDictionary; }
            set
            {
                colorDictionary = value;

            }
        }

        #endregion // [Properties]

        #region [Mothods]

        #region [Public]

        private void UpdateVisuals()
        {
            this.Children.Clear();
            double currentLength = Length;
            if (PlanPoint.Z < Length)
                currentLength = PlanPoint.Z;

            //пересчитать кисть.

            var cylinder = new Cylinder();
            cylinder.Material = MaterialHelper.CreateMaterial(CuttingBrush);
            cylinder.Slices = 100;
            cylinder.Point1 = new Point3D(0, 0, 0);
            cylinder.Point2 = new Point3D(0, 0, currentLength);
            cylinder.Radius1 = Diametr / 2;
            cylinder.Radius2 = Diametr / 2;


            this.Children.Add(cylinder);
        }

        public void OnPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged != null)
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        List<List<int>> temperatureList = new List<List<int>>();
        private void CreateData()
        {
            while (i < 200)
            {
                List<int> tlist = new List<int>();
                int tempI = i;
                for (int j = 0; j < 20; j++)
                {
                    if (j < 1 || j > 18)
                    {
                        tlist.Add(tempI - 40);
                    }
                    else
                    {
                        if (tempI > 0)
                        {
                            tlist.Add(tempI);
                            tempI -= 2;
                        }
                        else
                        {
                            tlist.Add(0);
                        }
                    }
                }
                temperatureList.Add(tlist);
                i++;
            }


        }

        public List<int> getTemperature(int time)
        {
            return temperatureList[time];
        }

        private List<int> getTemperature(int startTemp, int endTemp)
        {
            List<int> result = new List<int>();
            double stepTemp = (endTemp - startTemp) / 20.0;
            for (double i = startTemp; i > endTemp; i += stepTemp)
            {
                result.Add((int)i);
            }
            result[0] = result[0] - 40;
            result[19] = result[19] - 40;
            return result;
        }

        private void Run()
        {
            ni = 20; lbas = 3; lgt = 2 * lbas - 1;
            alg = 0.25; Nt = 75; R1 = 1.0; R2 = 1.5; hc = 3; hh = 0.5 * hc;
            m_1 = 3; m_2 = 3; m0 = 3;
            par[1, 1] = 1.089; par[1, 2] = 423; par[1, 3] = 2700;
            par[2, 1] = 1.700; par[2, 2] = 353; par[2, 3] = 8920;
            par[3, 1] = 0.691; par[3, 2] = 45; par[3, 3] = 7500;
            par[4, 1] = 1.090; par[4, 2] = 1.04; par[4, 3] = 2400;

            Input();
            al1 = alg / K1;
            al2 = alg / K3;
            dt = 1.0 / Nt;
            Move_t();
            Write_T_Rnormal(R2);
            Write_T_Znormal(hh);
        }

        private void UpdateTexture()
        {
            Bitmap bitmap = null;
            if (PlanPoint.Z < Length)
                bitmap = GetBitmap(Time, 1, (int)PlanPoint.Z + (int)(Length - PlanPoint.Z), true, (int)PlanPoint.Z);
            else
                bitmap = GetBitmap(Time, 1, (int)Length, false, (int)Length);

            var bitmapSoure = BitmapHelper.LoadBitmap(bitmap);
            CuttingBrush = new ImageBrush(bitmapSoure);
        }

        private Bitmap GetBitmap(int time, int width, int length, bool isCut, int cut)
        {
            SomeMethod();
            Bitmap b = new Bitmap(width, length);
            var map = getTemperature(time);
            if (isCut)
            {
                map = getTemperature(map[1], map[cut]);
            }
            else
                map = getTemperature(time);

            for (int i = 0; i < width; i++)
                for (int j = 0; j < length; j++)
                {
                    b.SetPixel(i, j, GetColorByTemperature(map[j]));
                }

            return b;
        }

        byte r = 250;
        byte g, b;
        bool isLast;

        private void SomeMethod()
        {
            int temperature = 80;

            while (g < 250 && !isLast)
            {
                g += 25;
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                temperature -= 2;

            }
            while (r > 0)
            {
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                r -= 25;
                temperature -= 2;

            }
            while (b < 250)
            {
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                b += 25;
                temperature -= 2;
            }
            while (g > 0)
            {
                isLast = true;
                ColorDictionary.Add(new KeyValuePair<int, System.Drawing.Color>(temperature, System.Drawing.Color.FromArgb(r, g, b)));
                g -= 25;
                temperature -= 2;

            }
        }

        private System.Drawing.Color GetColorByTemperature(double temperature)
        {
            for (int i = 0; i < ColorDictionary.Count; i++)
            {
                if (temperature < 2)
                    return System.Drawing.Color.Blue;
                if (temperature >= 80)
                    return System.Drawing.Color.Red;
                if (temperature >= ColorDictionary[i].Key)
                    return ColorDictionary[i - 1].Value;
            }
            return System.Drawing.Color.Black;
        }

        #region [Calculation]

        double[, ,] m_int = new double[150, 150, 30];
        double[, ,] f_bas = new double[150, 150, 30];
        double[,] matr = new double[150, 150];
        double[] D_Vector = new double[150];

        double[] p_i = new double[150];
        double[] w_i = new double[150];

        double[, ,] f = new double[150, 150, 30];
        double[, ,] ft = new double[150, 150, 30];

        double[] V_G = new double[150];
        double[] xt = new double[150];
        double[] bt = new double[150];
        double[] fl = new double[150];
        double[] fn = new double[150];
        double[] fv = new double[150];
        double[] fg = new double[150];
        double[] cond = new double[150];
        double[] r_one = new double[150];
        double[] zv = new double[150];

        double[,] a = new double[150, 150];
        double[,] ab = new double[150, 150];

        double[, ,] mw = new double[150, 150, 30];
        double[, ,] tm = new double[150, 150, 30];
        double[, ,] gm = new double[150, 150, 30];

        double dt, ss1, ss2, u1, u2, alg, al1, al2, K1, K2, K3, K4, R1, R2, hc, hh;
        int Nt, m_1, m_2, m0, ni, i, j, m, k, lbas, lgt;

        double[,] par = new double[10, 6];
        double[, ,] ta = new double[30, 30, 30];
        double[,] xv = new double[20, 20];
        double[,] yv = new double[20, 20];






        private void GaussSLAU(int nb, int ne, double[,] a, double[] f, ref double[] x)
        {
            double s, eps;
            int k, kr, l;

            eps = 1e-11;
            for (k = nb; k < ne; k++)
            {
                //if (Math.Abs(a[k, k]) < eps)
                //{
                //    l = k - 1;
                //    do
                //    {
                //        l = l + 1; kr = 1;
                //        if (Math.Abs(a[l, k]) > eps)
                //            kr = 0;

                //        if (l == ne)
                //            kr = 0;
                //    } while (kr == 0);
                //    for (j = nb; j < ne + 1; j++)
                //    {
                //        s = a[k, j]; a[k, j] = a[l, j]; a[l, j] = s;
                //    }
                //    s = f[k]; f[k] = f[l]; f[l] = s;
                //}
                for (i = k + 1; i <= ne; i++)
                {
                    s = a[i, k] / a[k, k];
                    a[i, k] = 0;
                    for (j = k + 1; j <= ne; j++)
                        a[i, j] = a[i, j] - s * a[k, j];
                    f[i] = f[i] - s * f[k];
                }
            }
            x[ne] = f[ne] / a[ne, ne];
            for (i = ne - 1; i >= nb; i--)
            {
                s = 0;

                for (k = i + 1; k <= ne; k++)
                    s = s + a[i, k] * x[k];

                x[i] = (f[i] - s) / a[i, i];
            }
        }

        private void Q_Ch(int n, ref double[] zero, ref double[] weight)
        {
            int m, i;
            double[,] a_ch = new double[150, 150];
            double[] b_ch = new double[150];

            for (m = 1; m <= n; m++)
            {
                zero[m] = -Math.Cos(Math.PI * m / (n + 1));
                b_ch[m] = 0;
                a_ch[1, m] = 1;
            }
            i = 1;
            do
            {
                b_ch[i] = 2.0 / i; i = i + 2;
            } while (i <= n);

            for (m = 2; m <= n; m++)
            {
                for (i = 1; i <= n; i++)
                {
                    a_ch[m, i] = a_ch[m - 1, i] * zero[i];
                }
            }
            GaussSLAU(1, n, a_ch, b_ch, ref weight);
        }

        private void Geom_Line(double a, double b, ref double[] x, ref  int n)
        {
            for (int i = 1; i <= n; i++)
                x[i] = 0.5 * ((b - a) * p_i[i] + a + b);
        }

        private void Input()
        {

            double s;
            Q_Ch(ni, ref p_i, ref w_i);
            fl[0] = 1;
            for (m = 1; m <= 50; m++)
                fl[m] = m * fl[m - 1];
            m = m0; s = 1 / par[m, 2];
            K1 = par[m_1, 2] * s;
            K3 = par[m_2, 2] * s;
            s = 1 / (par[m, 1] * par[m, 3]);
            K2 = par[m_1, 1] * par[m_1, 3] * s;
            K4 = par[m_2, 1] * par[m_2, 3] * s;
            for (m = 1; m <= ni; m++)
                r_one[m] = 1;
        }

        private void Int_Line(int ni, double[] f, double h, ref double s)
        {
            s = 0;
            for (int i = 1; i <= ni; i++)
                s = s + f[i] * w_i[i];
            s = 0.5 * h * s;
        }


        private void Form_Right_Sym(int mb, int me, int nf, double[, ,] f, double[] ri, ref double[] a, ref double w, ref double h)
        {
            double s = 0;

            double[] Li = new double[150];
            for (m = mb; m <= me; m++)
            {
                for (int j = 1; j <= ni; j++)
                {
                    Li[j] = ri[j] * f[nf, m, j] * cond[j];
                }
                Int_Line(ni, Li, h, ref s);
                a[m] = a[m] + w * s;
            }
        }

        private void Form_Matr_Sym(int mb, int me, int kb, int ke, int n, double[, ,] f, double[, ,] fi, ref double[,] a, ref double[] r, ref double w, ref double h)
        {
            double s = 0;

            double[] Li = new double[150];
            for (m = mb; m <= me; m++)
            {
                for (int k = kb; k <= ni; k++)
                {
                    for (int j = 1; j <= ni; j++)
                    {
                        Li[j] = r[j] * f[n, m, j] * fi[n, k, j];
                    }
                    Int_Line(ni, Li, h, ref s); a[m, k] = a[m, k] + w * s;
                }
            }
        }

        private void Bas_Pol_Ax(double t, double ro)
        {
            int m;
            double t2, t4, d, e, s;
            double[] p = new double[150];
            double[] v = new double[150];
            double[] g = new double[150];
            e = ro * ro;
            t2 = 1 - t * t;
            t4 = t2 * t2;
            //for (m = -2; m < 4; m++)
            //{

            //    fn[m + 3] = 0; fv[m + 3] = 0;
            //    fg[m + 3] = 0; g[m + 3] = 0;
            //}
            fn[0] = 1; fn[1] = ro * t; p[0] = 1; p[1] = t;
            v[0] = 1; v[1] = 3 * t; g[0] = 1; g[1] = 5 * t;

            for (m = 2; m <= lbas + 2; m++)
            {
                d = 1 / m;
                s = e / fl[m];
                p[m] = ((2 * m - 1) * p[m - 1] * t - (m - 1) * p[m - 2]) * d;
                v[m] = ((2 * m + 1) * v[m - 1] * t - (m + 1) * v[m - 2]) * d;
                g[m] = ((2 * m + 3) * g[m - 1] * t - (m + 3) * g[m - 2]) * d;
                fn[m] = s * p[m];
                fv[m] = s * t2 * v[m - 2];
                //fg[m + 3] = 3 * s * t4 * g[m - 1];
                //e = e * ro;
            }
        }

        private void T_P(double r, double z, int j)
        {
            double s, t, ro;
            double[] ft = new double[40];
            double[] fz = new double[40];
            double[] fr = new double[40];
            int q, m, k;

            ro = Math.Sqrt(r * r + z * z);
            t = z / ro;
            Bas_Pol_Ax(t, ro);
            f[1, 0, j] = 1; f[2, 0, j] = 0; f[3, 0, j] = 0;
            f[1, 1, j] = z; f[2, 1, j] = 0; f[3, 1, j] = 1; k = 2;

            for (m = 2; m <= lbas; m++)
            {
                s = fl[m];
                ft[m] = s * fn[m];
                fz[m] = s * fn[m - 1];
                fr[m] = -s * fv[m] / r;
                q = m + lbas - 1;
                ft[q] = s * fv[m];
                fz[q] = s * fv[m - 1];
                fr[q] = s * r * fn[m - 2];
                f[1, k, j] = ft[m];
                f[2, k, j] = fr[m];
                f[3, k, j] = fz[m];
                k = k + 1;
                f[1, k, j] = ft[q];
                f[2, k, j] = fr[q];
                f[3, k, j] = fz[q];
                k = k + 1;
            }
        }

        private void T_Normal_R(double hb, double he)
        {

            double h;
            double[] z = new double[150];

            Geom_Line(hb, he, ref z, ref ni);
            h = R2 * (he - hb);
            for (int j = 0; j < ni; j++)
            {
                T_P(R2, z[j], j);
                cond[j] = 1;
            }
            double www = 1;
            Form_Matr_Sym(0, lgt, 0, lgt, 1, f, f, ref a, ref r_one, ref www, ref h);
            Form_Right_Sym(0, lgt, 1, f, r_one, ref V_G, ref www, ref h);
        }

        private void Q_Normal_R(double Rw, double hb, double he)
        {

            double wg, h;
            double[] z = new double[150];
            wg = dt / K3;
            Geom_Line(hb, he, ref z, ref ni);
            h = Rw * (he - hb);
            for (int j = 1; j < ni; j++)
            {
                T_P(Rw, z[j], j); cond[j] = 1;
            }
            Form_Right_Sym(0, lgt, 1, f, r_one, ref V_G, ref wg, ref h);
        }

        private void Al_Normal_R(double al, double Rw, double hb, double he)
        {

            double wg, h;
            double[] z = new double[150];
            wg = dt * al;
            Geom_Line(hb, he, ref z, ref ni);
            h = R2 * (he - hb);
            for (int j = 1; j < ni; j++)
            {
                T_P(Rw, z[j], j);
                cond[j] = 1;
            }
            Form_Matr_Sym(0, lgt, 0, lgt, 1, f, f, ref a, ref r_one, ref wg, ref h);
        }

        private void Al_Normal_Z(double al, double Rin, double Rout, double zw)
        {
            double wg, h;
            double[] r = new double[150];
            wg = dt * al; h = Rout - Rin; Geom_Line(Rin, Rout, ref r, ref ni);
            for (int j = 1; j <= ni; j++)
            {
                T_P(r[j + 3], zw, j); cond[j] = 1;
            }
            Form_Matr_Sym(0, lgt, 0, lgt, 1, f, f, ref a, ref r, ref wg, ref h);
        }

        private void Area()
        {

            double[] r = new double[150];
            double[] z = new double[150];
            double[] L = new double[150];
            double Lw1, Lw2, hr, hz, t = 0;
            double s = 0;
            Geom_Line(-hh, hh, ref z, ref ni);
            hz = hc;
            Geom_Line(0, R1, ref r, ref ni);
            hr = R1;
            Lw1 = K1;
            Lw2 = K2;

            for (int kk = 1; kk <= 2; kk++)
            {
                for (int i = 1; i <= ni; i++)
                {
                    for (int j = 1; j <= ni; j++)
                        T_P(r[j], z[i], j);
                    for (int m = 0; m <= lgt; m++)
                    {
                        for (int k = 0; k <= lgt; k++)
                        {
                            for (int j = 1; j <= ni; j++)
                                L[j] = r[j] * f[1, m, j] * f[1, k, j];
                            Int_Line(ni, L, hr, ref s);
                            tm[m, k, i] = s;
                            for (int j = 1; j <= ni; j++)
                                L[j] = r[j] * (f[2, m, j] * f[2, k, j] + f[3, m, j] * f[3, k, j]);
                            Int_Line(ni, L, hr, ref t);
                            gm[m, k, i] = t;
                        }
                    }
                }
                for (int m = 0; m <= lgt; m++)
                {
                    for (int k = 0; k <= lgt; k++)
                    {
                        for (int j = 1; j <= ni; j++)
                            L[j] = tm[m, k, j];

                        Int_Line(ni, L, hz, ref s);
                        ab[m, k] = ab[m, k] + Lw1 * s;

                        for (int j = 1; j <= ni; j++)
                            L[j] = gm[m, k, j];

                        Int_Line(ni, L, hz, ref t);
                        a[m, k] = a[m, k] + Lw2 * t;
                    }
                }
                Geom_Line(R1, R2, ref r, ref ni);
                hr = R2 - R1;
                Lw1 = K3;
                Lw2 = K4;
            }
            //for (int k = 0; k <= lgt; k++)
            //    for (int m = 0; m <= lgt; m++)
            //        a[k, m] = dt * a[k, m] + ab[k, m];
        }

        private void Convert_Z()
        {
            double t;
            double[] tw = new double[150];
            double[] al = new double[150];
            double[] z = new double[150];
            string ff;

            Geom_Line(-hh, hh, ref z, ref ni);
            for (int i = 1; i <= ni; i++)
            {
                T_P(R1, z[i], i);
                tw[i] = 0;
                for (int m = 0; m <= lgt; m++)
                    tw[i] = tw[i] + xt[m] * f[1, m, i];
            }
            for (int i = 1; i < ni; i++)
            {
                for (int j = 1; j < ni; j++)
                {
                    xv[i, j] = Math.Cos(al[i]) * R2;
                    yv[i, j] = Math.Sin(al[i]) * R2;
                    ta[i, j, i] = tw[j];
                }
            }
        }

        private void Convert_R(double zw)
        {
            double[] al = new double[3];
            double[] x = new double[150];
            double[] y = new double[150];
            double[] r = new double[150];
            double[] tw = new double[150];

            Geom_Line(0, 2 * Math.PI, ref al, ref ni);
            Geom_Line(0, R2, ref r, ref ni);

            for (int j = 1; j <= ni; j++)
            {
                tw[j] = 0;
                T_P(r[j], zw, j);
                for (int m = 0; m <= lgt; m++)
                    tw[j] = tw[j] + xt[m] * f[1, m, j];
                zv[j] = zw;
            }
            for (int i = 1; i < ni; i++)
            {
                for (int j = 1; j < ni; j++)
                {
                    xv[i, j] = Math.Cos(al[i]) * r[j];
                    yv[i, j] = Math.Sin(al[i]) * r[j];
                    ta[i, j, i] = tw[j];
                }
            }
        }

        private void Move_t()
        {
            int i, p, ll;
            double[] x = new double[150];
            double[] ta = new double[150];
            double t, s, s1, s2;
            string ff;
            t = 0;
            Area();
            Q_Normal_R(R2, -hh, hh);

            Al_Normal_Z(al1, 0, R1, hh);
            Al_Normal_Z(al1, 0, R1, -hh);
            Al_Normal_Z(al2, R1, R2, hh);
            Al_Normal_Z(al2, R1, R2, -hh);

            for (int L = 1; L <= 39; L++)
            {
                for (int m = 0; m <= lgt; m++)
                {
                    bt[m] = V_G[m];
                    for (int k = 0; k <= lgt; k++)
                        bt[m] = bt[m] + ab[m, k] * xt[k];

                }
                GaussSLAU(0, lgt, a, bt, ref xt);
                Convert_R(-hh);
                Convert_R(hh);
                Convert_Z();
                t = t + dt;
            }
            //запись в файл

        }

        private void Write_T_Rnormal(double R)
        {
            double t = 0;
            double[] z = new double[150];
            string ff = "";

            Geom_Line(-hh, hh, ref z, ref ni);
            for (int i = 1; i <= ni; i++)
            {
                T_P(R, z[i], i);
                t = 0;
                for (int m = 0; m <= lgt; m++)
                {
                    t = t + xt[m] * f[2, m, i];
                    ff += t.ToString();
                    //writeln(ff,t:8:5);
                }

                for (int m = 0; m < lgt; m++)
                {
                    // writeln(ff, m, ' ', xt[m]); Close(ff);
                }
            }
        }

        private void Write_T_Znormal(double zw)
        {
            double t = 0;
            double[] r = new double[150];
            string ff;

            //Writeln(ff,'      ', m_1,'     ',m_2);
            //Writeln(ff,'          T                        r         ');

            Geom_Line(0, R1, ref r, ref ni);
            for (int j = 1; j < ni; j++)
            {
                t = 0;
                T_P(r[j], zw, j);
                for (int m = 0; m < lgt; m++)
                {
                    t = t + xt[m] * f[1, m, j];
                    //writeln(ff,t,'  ',r[j]);
                }
            }
            Geom_Line(R1, R2, ref  r, ref ni);
            for (int j = 1; j < ni; j++)
            {
                t = 0;
                T_P(r[j], zw, j);
                for (int m = 0; m < lgt; m++)
                {
                    t = t + xt[m] * f[1, m, j];
                    //writeln(ff,t,'  ',r[j]);
                }
            }
            //Close(ff);
        }

        #endregion [Calculation]
      

        #endregion // [Mothods]
      
    }
}
