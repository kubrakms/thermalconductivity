﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;

namespace ThermalConductivity.Models
{
    public abstract class BaseModel : MeshElement3D, INotifyPropertyChanged
    {
        #region  [ INotifyPropertyChanged Implementation]
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion // [ INotifyPropertyChanged Implementation]

        #region [Fields]

        private double width = 1;
        private double height = 1;
        private double length =1;
        private Point3D center = new Point3D(0,0,0);

        private Visual3D model = null;

        #endregion // [Fields]

        #region [Properties]

        public double Width
        {
            get { return width; }
            set
            {
                width = value;
                CreateModel();
            }
        }

        public double Height
        {
            get { return height; }
            set
            {
                height = value;
                CreateModel();
            }
        }

        public double Length
        {
            get { return length; }
            set
            {
                length = value;
                CreateModel();
            }
        }

        public Point3D Center
        {
            get { return center; }
            set
            {
                center = value;
                CreateModel();
            }
        }

        public Visual3D Model
        {
            get 
            {
                return model; 
            }
            protected set
            {
                model = value;
                OnPropertyChanged("Model");
            }
        }

        #endregion // [Properties]

        #region [Methods]

        protected abstract void CreateModel();

        #endregion // [Methods]

        
    }
}
